# eucrawl

* Python based Project build with the 'Scrapy' Framework
* Crawls and downloads the European Parliament member information
* Analyses the gender relation

# Installation

### PyCharm
Clone the repository and run it as a PyCharm Project in a virtual env. (recommended)

### Packages 

Make sure all Packages are installed:

*   scrapy (https://pypi.org/project/Scrapy/)
*   pandas (https://pypi.org/project/pandas/)
*   matplotlib (https://pypi.org/project/matplotlib/)
*   guender-guesser (https://pypi.org/project/gender-guesser/)
*   pick (https://pypi.org/project/pick/)
*   pymongo (https://pypi.org/project/pymongo/)


## Usage


``` bash
# Navigate to the eucrawl folder
$ cd eucrawl/

# Download the data and save as csv
$ Scrapy crawl member -o member.csv -t csv

# Start the analysing script
$ python3 analyse_interface.py

```


## Enable MongoDB Connection

By default the MongoDB connection is disabled.
To enable a MongoDB connection use the following lines inside the Settings.py file:

``` bash
# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
 ITEM_PIPELINES = {
  'eucrawl.pipelines.EucrawlPipeline': 300,
 }
```

## Configure MongoDB Connection

To configure the MongoDB Connection change the following lines inside the Pipelines.py file:

``` bash
def __init__(self):
    self.conn = pymongo.MongoClient(        # connection to MongoDB
        'localhost',
        27017
    )
    db = self.conn['eu_delegates']               # Database name
    self.collection = db['eu_delegates_table']   # Database Table name
```


