# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo

class EucrawlPipeline(object):
    def __init__(self):
        self.conn = pymongo.MongoClient(        # connection zu MongoDB
            'localhost',
            27017
        )
        db = self.conn['eu_delegates']               # Name der Datenbank
        self.collection = db['eu_delegates_table']   # Tabelle innerhalb der Datenbank

    def process_item(self, item, spider):
        self.collection.insert(dict(item))
        return item
