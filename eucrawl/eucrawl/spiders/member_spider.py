import scrapy

class EuroSpider(scrapy.Spider):
    name = "member"
    start_urls = [
        'http://www.europarl.europa.eu/meps/en/full-list/all',   # Einstiegspunkt
    ]

    def parse(self, response):
            for href in response.css('.single-member-container a[href*=meps]::attr(href)'):
                yield response.follow(href, self.parse_member)

    def parse_member(self, response):
        def extract_with_css(query):
            return response.css(query).get(default='').strip()

        kommitee = response.xpath("//abbr/text()").getall()
        group = response.css('#erpl-political-group-name .ep_name::text').get()
        party = response.css('.ep-layout_border div.ep-layout_level2 .ep_name::text').get()
        country = response.css('#erpl-member-country-name::text').get()

        yield {
            'name': extract_with_css('.erpl-member-card-full-member-name::text'),
            'group': group,
            'party': party,
            'country': country,
            'kommitee': kommitee
        }
    # in CSV Abspeichern
    # scrapy crawl member -o member.csv -t csv