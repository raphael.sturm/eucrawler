#!/usr/bin/python
# -*- coding: latin-1 -*-

import pandas as pd
import matplotlib.pyplot as plt
import gender_guesser.detector as gender
from pick import pick

d = gender.Detector()
name_sort = []

df = pd.read_csv("member.csv", encoding="utf-8-sig") # Reading the dataset in a dataframe using Pandas

party_count = df['party'].value_counts()
group_count = df['group'].value_counts()
country_count = df['country'].value_counts()
name_count = df['name'].value_counts()
name_sort = df['name'].sort_values()

firstname = df['name'].str.split(' ').str[0] # Splitting the Names for analysing the Firstname w/ gender-guesser library

gender = []

for col in firstname:
    gender.append(d.get_gender(col)) # Using gender-guesser library

df["gender"] = gender

######## Gender User Choice Parlament ############

### input Analyse ###
index = 5
gender_percentage = []
user_party_selection = []
user_group_selection = []
choice = 'start'

def choose():
    from pick import pick
    title = " Welcome! This script allows you to analyse the gender relation of the European Parliament. \n \n Usage: \n 1. Choose what you would like to analyse. For navigation use the arrow keys: UP [^] and DOWN [v]. \n 2. To get back to the Menu just close the Plot-Window. (It is possible to save/download the Plot). \n 3. To finish the script choose 'Exit' \n\n Menu: \n\n >> What do you would like to analyse?"
    options = ['Gender releation of the whole parliament', 'Gender relation of the countries', 'Gender relation of the parties', 'Gender relation of the Groups', 'Exit']
    global index
    choice, index = pick(options, title)

    if index == 0:
        complete()

    if index == 1:
        countries()

    if index == 2:
        party()

    if index == 3:
        group()

    if index == 4:
        quit()

def group():
    title = 'Please choose a Group to analyse: '
    options = ['Group of the Alliance of Liberals and Democrats for Europe', "Group of the European People", "Europe of Nations and Freedom Group", "Group of the Greens/European Free Alliance", "Europe of Freedom and Direct Democracy Group", "Confederal Group of the European United Left - Nordic Green Left", "Group of the Progressive Alliance of Socialists and Democrats in the European Parliament", "European Conservatives and Reformists Group" ]
    option, index = pick(options, title)

    user_group_input = option
    global user_group_selection
    user_group_selection = (df.group.str.contains(user_group_input, regex=True))
    df["User Selection"] = user_group_selection

    users_group_choice = df[df["User Selection"] == True]

    users_group_gender_types_count = users_group_choice['gender'].value_counts()   # Count all different Gender Types of Gender Geuesser
    users_group_gender_types = users_group_choice['gender'].value_counts().index  # !Gets the Index of the Panda Series. Shows the Types of Gender.
    users_group_gender_percentage = (users_group_choice.gender.value_counts(normalize=True)) # Compute Percent of Gender Types

    labels = users_group_gender_types
    data = users_group_gender_percentage

    fig1, ax1 = plt.subplots()
    ax1.pie(data, labels=labels, autopct='%1.1f%%',
            shadow=False, startangle=90, labeldistance=1.05)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    ax1.set_title(user_group_input + ": Gender Percentage")

    fig1.tight_layout()
    plt.show()
    choose()

def party():
    title = 'Please choose a Party to analyse: '
    options = ['Sozialdemokratische Partei Deutschlands', 'Christlich Demokratische Union Deutschlands', 'DIE LINKE.',
               'Alternative f�r Deutschland', 'B�ndnis 90/Die Gr�nen', 'Die PARTEI', 'Freie Demokratische Partei' , '�kologisch-Demokratische Partei', 'Christlich-Soziale Union in Bayern e.V', 'B�ndnis C']
    option, index = pick(options, title)

    user_party_input = option
    global user_party_selection
    user_party_selection = (df.party.str.contains(user_party_input, regex=True))
    df["User Selection"] = user_party_selection

    users_party_choice = df[df["User Selection"] == True]

    users_party_gender_types_count = users_party_choice[
        'gender'].value_counts()  # Count all different Gender Types of Gender Geuesser
    users_party_gender_types = users_party_choice[
        'gender'].value_counts().index  # !Gets the Index of the Panda Series. Shows the Types of Gender.
    users_party_gender_percentage = (
        users_party_choice.gender.value_counts(normalize=True))  # Compute Percent of Gender Types

    fig1, ax1 = plt.subplots()
    ax1.pie(users_party_gender_percentage, labels=users_party_gender_types, autopct='%1.1f%%',
            shadow=True, startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    ax1.set_title(user_party_input + ": Gender Percentage")
    fig1.tight_layout()
    plt.show()
    choose()


def complete():
    global gender_percentage
    gender_percentage = (df.gender.value_counts(normalize=True))

    # Pie chart, where the slices will be ordered and plotted counter-clockwise:
    labels = 'male', 'female', 'unkown', 'mostly_male', 'mostly_fmale', 'andy'
    explode = (0.1, 0.1, 0.1, 0.1, 0.1, 0.1)  # only "explode" the 2nd slice (i.e. 'Hogs')

    fig1, ax1 = plt.subplots()
    ax1.pie(gender_percentage, explode=explode, labels=labels, autopct='%1.1f%%',
            shadow=True, startangle=90,)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

    ax1.set_title("EU Parlament Gender Percentage")
    fig1.tight_layout()
    plt.show()
    choose()

def countries():
    title = 'Please choose a Country to analyse: '
    options = ['Austria', 'Belgium', 'Bulgaria', 'Croatia', 'Cyprus', 'Czechia', 'Denmark', 'Estonia', 'Finland', 'France', 'Germany', 'Greece', 'Hungary', 'Ireland', 'Italy', 'Latvia', 'Lithuania', 'Luxembourg', 'Malta', 'Netherlands', 'Poland', 'Portugal', 'Romania', 'Slovakia', 'Slovenia', 'Spain', 'Sweden', 'United Kingdom']
    option, index = pick(options, title)

    user_country_input = option
    global user_country_selection
    user_country_selection = (df.country.str.contains(user_country_input, regex=True))
    df["User Selection"] = user_country_selection

    users_country_choice = df[df["User Selection"] == True]

    users_country_gender_types_count = users_country_choice[
        'gender'].value_counts()  # Count all different Gender Types of Gender Geuesser
    users_country_gender_types = users_country_choice[
        'gender'].value_counts().index  # !Gets the Index of the Panda Series. Shows the Types of Gender.
    users_country_gender_percentage = (
        users_country_choice.gender.value_counts(normalize=True))  # Compute Percent of Gender Types

    fig1, ax1 = plt.subplots()
    ax1.pie(users_country_gender_percentage, labels=users_country_gender_types, autopct='%1.1f%%',
            shadow=True, startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    ax1.set_title(user_country_input + ": Gender Percentage")
    fig1.tight_layout()
    plt.show()
    choose()

if index == 5:
    choose()
